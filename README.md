Omitsis Android 
===============

Hello World!


In order to complete this test you have to follow these steps:

1) Fork this repository
-----------------------
Fork this repository to your computer or just download the source.

2) List companies
-----------------
Create a list of the companies.
We have published a json where you can retrieve all the information of the companies for you:
http://www.json-generator.com/api/json/get/caZrciQZRu?indent=2

3) Detail
---------
Make a view with the detail information of the company when the user taps on one element of the list. Feel free to add your own features.

6) Finish
--------
Send us your public repository link to check the solution, or just email us with your source code: mobile[at]omitsis[dot]com.


Good luck!